# Web-Stack

## Directories
### Mapping Source
In the docker-compose.yml, the volume for php and nginx needs to change
``` yaml
# mount the entire project in to src on the virtual
    volumes:
      - ./:/src
# mount a sub folder that holds the application source
    volumes:
      - ./app:/src
```
### Mapping Web Root
Edit the site.conf file to change the web root for nginx, this should be an absolute path from the /src directory.
``` bash
# serve the public sub directory
    root /src/public;
```
## Port Forwarding
By default nginx is listening to http on 8180. Add another port for https or change the ports to forward.
``` yaml
    ports:
      - "8180:80" #http
#      - "8443:443" #https
```
## Server Hostname
By default server is setup to answer on 127.0.0.1. This can be changed by editing the docker-compose and the site.conf.
``` yaml
    environment:
      - VIRTUAL_HOST=127.0.0.1 #Hostname
      - VIRTUAL_PORT=80
```
``` bash
    server_name 127.0.0.1; #nginx hostname
```
localhost is preconfigured on nginx and was not removed, if you want to use that you will need to change it on the image.
## Reverse Proxy
I have been using jwilder's nginx proxy, "jwilder/nginx-proxy". This proxy will proxy to images with certain environment variables set.
``` yaml
    environment:
      - VIRTUAL_HOST=127.0.0.1  #Forwards this address to this container
      - VIRTUAL_PORT=80         #Force use this port
```
The reverse proxy will need to be on the same network. Locate the network that you need and then attach the proxy container to it.\
``` bash 
docker network list 
docker network connect {network} {container}
```
## SSL Certificates
I have not had a chance to set this up yet but using the proxy will allow certs from it to pass to the proxied servers.